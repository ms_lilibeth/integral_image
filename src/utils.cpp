#include "utils.h"
#include <future>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "opencv2/imgcodecs.hpp"

using namespace std;


vector<AnnotatedImage> read_images(vector<string> paths, uint threads_num){
    if(paths.empty()){
        return {};
    }
    vector<AnnotatedImage> images(paths.size());
    auto read_fn = [](string&& path){
        return AnnotatedImage{cv::imread(path), path};
    };

    if(paths.size() == 1 || threads_num <= 1){
        transform(make_move_iterator(paths.begin()),
                  make_move_iterator(paths.end()),
                  images.begin(),
                  read_fn);
    } else {
        std::vector<std::future<AnnotatedImage>> futures;
        futures.reserve(threads_num);
        auto wait = [&futures, &images](){
            for(auto& f: futures){
                images.push_back(f.get());
            }
        };

        for(auto& path: paths){
            futures.push_back( async(read_fn, move(path)));
            if(futures.size() == threads_num){
                wait();
                futures.clear();
            }
        }
        wait();
    }

    auto del_it = remove_if(images.begin(), images.end(),
                            [](const AnnotatedImage& a_img){
                                if(a_img.mat.empty()){
                                    cout << "Failed to read image: " << a_img.path << '\n';
                                    return true;
                                }
                                return false;
                            });
    images.erase(del_it, images.end());
    return images;
}

void img2file(const AnnotatedImage& img){
    ofstream output_stream;
    output_stream.open(img.path.data());
    if (output_stream.bad()){
        throw runtime_error("Failed to open file for writing: " + img.path);
    }
    cv::Mat float_mat;
    uchar depth = img.mat.type() & CV_MAT_DEPTH_MASK;
    if(depth != CV_32F){
        img.mat.convertTo(float_mat, CV_32F);
    } else {
        float_mat = img.mat;  // just copies a header, not so fat
    }
    std::vector<cv::Mat1f> channels_received;
    cv::split(float_mat, channels_received);
    for(const auto& channel: channels_received){
        for(int row_idx = 0; row_idx < channel.rows; row_idx++)
        {
            const float* row = channel.ptr<float>(row_idx);
            for(int col_idx = 0; col_idx < channel.cols; col_idx++){
                output_stream << row[col_idx] << ' ';
            }
            output_stream << '\n';
        }
        output_stream << '\n';
    }
    output_stream.close();
}

void images2file(const std::vector<AnnotatedImage>& images, uint threads_num){
    if(images.empty()){
        return;
    } else if(images.size() == 1 || threads_num <= 1){
        for(const auto& img: images){
            img2file(img);
        }
    } else {
        std::vector<std::future<void>> futures;
        futures.reserve(threads_num);

        auto wait = [&futures](){
            for(auto& f: futures){
                f.get();
            }
        };

        for(const auto& img: images){
            futures.push_back(
                    async(img2file, img)
            );
            if(futures.size() == threads_num){
                wait();
                futures.clear();
            }
        }
        wait();

    }
}
