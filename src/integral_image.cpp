#include "integral_image.h"
#include "opencv2/imgcodecs.hpp"
#include <future>
#include <vector>

/* Integral image is calculated in two passes: partial sum over rows, then - partial sum over columns (or vise-a-versa,
 * it makes no sense). For example:
 * (1) sum left -> right
 * 0 1 2 3  => 0 1 3 6
 * 0 1 2 3     0 1 3 6
 *
 * (2) sum up -> down
 * 0 1 3 6  => 0 1 3 6
 * 0 1 3 6     0 2 6 12
 * */

void rows_partial_sum(cv::Mat img){
    // partial sum over a row
    /*
     * 0 1 2 3  => 0 1 3 6
     * 0 1 2 3     0 1 3 6
     * */
    for(int col_idx = 1; col_idx < img.cols; col_idx++){
        img.col(col_idx) += img.col(col_idx - 1);
    }
}

void cols_partial_sum(cv::Mat img){
    /*
     * 0 1 2 3  => 0 1 2 3
     * 0 1 2 3     0 2 4 6
     * */
    for(int row_idx=1; row_idx<img.rows; ++row_idx){
        img.row(row_idx) += img.row(row_idx - 1);
    }
}

void parallelize_over_dim(const cv::Mat &img, uint threads_num, int dim){
    // Compute rows/columns partial sum in parallel
    uint dim_size;
    std::function<void(uint, uint)> process_part;
    if(dim == 0){
        dim_size = img.rows;
        process_part = [img](uint row_from, uint row_to){
            rows_partial_sum(img.rowRange(row_from, row_to));
        };
    } else if (dim == 1){
        dim_size = img.cols;
        process_part = [img](uint col_from, uint col_to){
            cols_partial_sum(img.colRange(col_from, col_to));
        };
    } else {
        throw std::logic_error("dim: expected 0 or 1, got " + std::to_string(dim));
    }
    uint n_parts = threads_num < dim_size ? threads_num : dim_size;
    uint part_size = threads_num < dim_size ? dim_size / threads_num : 1;
    std::vector<std::future<void>> futures;
    futures.reserve(n_parts);
    for(uint part_n=0; part_n < n_parts; ++part_n){
        uint idx_from = part_n * part_size;
        // if the last part is processed, take all rows till the end; else - till the next part
        uint idx_to = (part_n == n_parts - 1) ? dim_size : ((part_n + 1) * part_size);
        futures.push_back(
                std::async(process_part, idx_from, idx_to)
        );
    }
    for(auto& f: futures){
        f.get();
    }
}

cv::Mat compute_integral_image(const cv::Mat &img, uint threads_num) {
    cv::Mat output;
    img.convertTo(output, CV_32F);
    if(threads_num <= 1){
        rows_partial_sum(output);
        cols_partial_sum(output);
    } else{
        // parallelize over rows
        parallelize_over_dim(output, threads_num, 0);
        // parallelize over columns
        parallelize_over_dim(output, threads_num, 1);
    }
    return output;
}
