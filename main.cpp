#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <thread>

#include <boost/program_options.hpp>

#include "integral_image.h"
#include "utils.h"

namespace po = boost::program_options;
using namespace std;

int main(int ac, char **av) {
    uint threads_num;
    vector<string> img_paths;
    po::options_description desc("Options:");
    desc.add_options()
            ("help", "produce help message")
            ("image,i",
                    po::value(&img_paths)->composing(),
                    "absolute path to input image (multiple images can be passed)")
            ("threads,t",
                    po::value(&threads_num)->default_value(0),
                    "the number of threads you recommend to use; if 0, number of cpu cores will be used instead")
            ;

    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << "\n";
        return 0;
    }
    if(img_paths.empty()){
        cout << "At least one input image should be specified" << endl;
        return 0;
    }
    vector<AnnotatedImage> images = read_images(move(img_paths));
    if (images.empty()){
        std::cout << "No images to process" << std::endl;
        return 0;
    }
    if(threads_num == 0){
        threads_num = std::thread::hardware_concurrency();
    }
    cout << "Executing in " << threads_num << " thread(s)" << endl;
    vector<AnnotatedImage> integral_images;
    integral_images.reserve(images.size());
    for(auto& source_img: images){
        integral_images.push_back({ compute_integral_image(source_img.mat, threads_num), source_img.path + ".integral"});
    }
    images2file(integral_images, threads_num);
    cout << "Done. Output images: " << '\n';
    for(const auto& img: integral_images){
        cout << img.path << '\n';
    }
    return 0;
}
