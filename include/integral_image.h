#pragma once
#include <opencv2/core/mat.hpp>

/** Compute integral image sequentially or in multi-thread mode
 * @param img source OpenCV image of any type (values will be casted to float)
 * @param threads_num  0 or 1 if result should be calculated sequentially, otherwise - the number of threads to use
 * */
cv::Mat compute_integral_image(const cv::Mat &img, uint threads_num=1);