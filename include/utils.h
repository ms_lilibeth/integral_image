#pragma once
#include "opencv2/imgproc.hpp"
#include <string>
#include <vector>
#include <utility>

/** An OpenCV image annotated with file path */
struct AnnotatedImage{
    cv::Mat mat;
    std::string path;
};

/** Read images from files
 * @param paths vector of paths to images; images that could not be read will be skipped
 * @param threads_num 0 or 1 if images should be read sequentially, otherwise - the number of threads to use
 * @returns OpenCV images annotated with their source paths
 * */
std::vector<AnnotatedImage> read_images(std::vector<std::string> paths, uint threads_num=1);

/** Export integral image to text file.
 * @details The text file has the following format: channels are separated with double new line, values in channel are separated with one space,
 * rows in channel - with one new line
 * @param img OpenCV image annotated with output path
 * @warning img.mat is expected to contain float values, otherwise type cast may return an unexpected result
 * */
void img2file(const AnnotatedImage& img);

/** Export multiple integral images to text files.
 * @details (about the format)
 * @param images images annotated with output paths
 * @param threads_num 0 or 1, if images should be saved sequentially, otherwise - the number of threads to use
 * */
void images2file(const std::vector<AnnotatedImage>& images, uint threads_num);

