#pragma once

#include <filesystem>
#include <string>
#include <tuple>

#include "opencv2/imgproc.hpp"

/// ValuesEqual OpenCV Mat equality
bool mat_are_equal(const cv::Mat &mat1, const cv::Mat &mat2, double tolerance = 0);

/// Create human-readable OpenCV::Mat type name; ex. 8UC1 for 8-bit char image with one channel
std::string cv_type2str(int type);

namespace fs = std::filesystem;

class TestResources {
private:
    static std::filesystem::path test_resources_folder_;

public:
    static std::filesystem::path test_resources_folder();
    static void test_resources_folder(const std::string& test_resources_path);

    /// Get absolute path from the path relative to project's tests/resources folder
    /// Raises runtime_error if file does not exist
    static std::string get_abs_resource_path(const std::string &relative_path);
};

std::tuple<bool, std::string> text_matches_image(std::istream& input_stream, const cv::Mat3f &source_img, double tolerance=1e-5);

std::string datetime2str();