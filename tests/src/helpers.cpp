#include "helpers.h"
#include <sstream>
#include <ctime>

bool mat_are_equal(const cv::Mat &mat1, const cv::Mat &mat2, double tolerance) {
// Source: https://stackoverflow.com/questions/9905093/how-to-check-whether-two-matrices-are-identical-in-opencv

// treat two empty mat as identical as well
    if (mat1.empty() && mat2.empty()) {
        return true;
    }
// if dimensionality is not identical, these two mat are not identical
    if (mat1.cols != mat2.cols || mat1.rows != mat2.rows || mat1.dims != mat2.dims) {
        return false;
    }
    cv::Mat diff;
    cv::absdiff(mat1, mat2, diff);
    double diff_min, diff_max;
    cv::minMaxLoc(diff, &diff_min, &diff_max);
    return diff_max <= tolerance;
}

std::string cv_type2str(int type) {
    // Source: https://stackoverflow.com/questions/10167534/how-to-find-out-what-type-of-a-mat-object-is-with-mattype-in-opencv
    std::string r;

    uchar depth = type & CV_MAT_DEPTH_MASK;
    uchar channels = 1 + (type >> CV_CN_SHIFT);

    switch (depth) {
        case CV_8U:
            r = "8U";
            break;
        case CV_8S:
            r = "8S";
            break;
        case CV_16U:
            r = "16U";
            break;
        case CV_16S:
            r = "16S";
            break;
        case CV_32S:
            r = "32S";
            break;
        case CV_32F:
            r = "32F";
            break;
        case CV_64F:
            r = "64F";
            break;
        default:
            r = "User";
            break;
    }

    r += "C" + std::to_string(channels);
    return r;
}

std::filesystem::path TestResources::test_resources_folder_;

std::filesystem::path TestResources::test_resources_folder() { return test_resources_folder_; }

void TestResources::test_resources_folder(const std::string &test_resources_path) {
    if (!test_resources_folder_.empty()) {
        throw std::runtime_error("Test resource folder path has already been provided");
    }
    test_resources_folder_ = fs::path(test_resources_path);
    if (!fs::exists(test_resources_folder_)) {
        throw std::invalid_argument("Test resources folder does not exist: " + test_resources_path);
    }
    if (!fs::is_directory(test_resources_folder_)) {
        throw std::invalid_argument(test_resources_path + " is not a directory");
    }
}

std::string TestResources::get_abs_resource_path(const std::string &relative_path) {

    namespace fs = std::filesystem;
    auto abs_path = test_resources_folder_ / relative_path;
    if (fs::exists(abs_path)) {
        return abs_path.string();
    }
    throw std::runtime_error("File was not found: " + abs_path.string());
}

std::tuple<bool, std::string> text_matches_image(std::istream& input_stream, const cv::Mat3f &source_img,
                                                 double tolerance) {
    for (int channel_idx = 0; channel_idx < source_img.channels(); ++channel_idx) {
        for (int row_idx = 0; row_idx < source_img.rows; ++row_idx) {
            for (int col_idx = 0; col_idx < source_img.cols; ++col_idx) {
                float value;
                input_stream >> value;
                if (!input_stream) {
                    std::ostringstream error_msg;
                    error_msg << "Failed to read value (" << row_idx << ',' << col_idx << ')';
                    return {false, error_msg.str()};
                }
                float expected = source_img[row_idx][col_idx][channel_idx];
                if(value - expected > tolerance){
                    std::ostringstream error_msg;
                    error_msg << '(' << row_idx << ',' << col_idx << "), channel " << channel_idx
                        << ": expected " << expected << ", got " << value;
                    return {false, error_msg.str()};
                }
            }
        }
    }
    return {true, ""};
}

std::string datetime2str(){
    time_t rawtime;
    struct tm * timeinfo;
    char buffer[80];

    time (&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer,sizeof(buffer),"%d-%m-%Y_%H:%M:%S",timeinfo);
    return {buffer};
}