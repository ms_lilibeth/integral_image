#include <vector>
#include <tuple>
#include <sstream>

#include "gtest/gtest.h"
#include "opencv2/imgcodecs.hpp"
#include "integral_image.h"
#include "helpers.h"

struct TestImages {
    cv::Mat source_img;
    cv::Mat integral_img;
};

class TestIntegralOfOneChannelImage :
        public testing::TestWithParam<std::tuple<TestImages, uint>> {
};  // parametrize with (images - threads num)

std::string ptest_name_generator(const testing::TestParamInfo<TestIntegralOfOneChannelImage::ParamType> &info) {
    //  test names must be non-empty, unique, and may only contain ASCII alphanumeric characters. In particular, they should not contain underscores
    // outputs <mat type>threads<threads num>, ex.: 8UC1threads2
    std::ostringstream name;
    auto[img_pair, threads_num] = info.param;
    name << cv_type2str(img_pair.source_img.type()) << "threads" << threads_num;
    return name.str();
}

INSTANTIATE_TEST_SUITE_P(
        SingleChannelImgParametrization,
        TestIntegralOfOneChannelImage,
        testing::Combine(  // Combine input images with number of threads
                // images
                testing::Values(
                        TestImages{  // 4x4, unsigned char
                                (cv::Mat_<uchar>(4, 4) << 0, 1, 2, 3, 1, 2, 3, 4, 2, 3, 4, 5, 3, 4, 5, 6),
                                (cv::Mat_<float>(4, 4) << 0, 1, 3, 6, 1, 4, 9, 16, 3, 9, 18, 30, 6, 16, 30, 48)
                        },
                        TestImages{  // 4x4, int16
                                (cv::Mat_<short>(4, 4) << 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3),
                                (cv::Mat_<float>(4, 4) << 0, 1, 3, 6, 0, 2, 6, 12, 0, 3, 9, 18, 0, 4, 12, 24)
                        },
                        TestImages{  // 4x3, double
                                (cv::Mat_<float>(4, 3) << 0.2, 0.1, 0.3, 0, 0.5, 0, 0.1, 0.5, 0.4, 0.3, 0.1, 0.3),
                                (cv::Mat_<float>(4, 3) << 0.2, 0.3, 0.6, 0.2, 0.8, 1.1, 0.3, 1.4, 2.1, 0.6, 1.8, 2.8),
                        },
                        TestImages{  // 3x4, int with negative values
                                (cv::Mat_<int>(3, 4) << 2, 0, 1, 3, -1, -5, 5, 1, 3, 0, 4, 3),
                                (cv::Mat_<float>(3, 4) << 2, 2, 3, 6, 1, -4, 2, 6, 4, -1, 9, 16)
                        }
                ),
                // threads num
                testing::Values(0u, 1u, 2u, 3u, 4u)
        ), // eof Combine
// human-readable test names generator
        ptest_name_generator

); // eof INSTANTIATE_TEST_SUITE_P

TEST_P(TestIntegralOfOneChannelImage, TestSuccessfulImgProcessing) {
    auto[img_pair, threads_num] = GetParam();
    cv::Mat received = compute_integral_image(img_pair.source_img, threads_num);
    double tolerance = 1e-4;
    ASSERT_TRUE(mat_are_equal(img_pair.integral_img, received, tolerance))
                                << "Integral image received: " << cv::format(received, cv::Formatter::FMT_NUMPY);
}

// TODO: add more multi-channels tests (rows num != columns num, 4-ch. image)
TEST(TestIntegralOfMultipleChannelsImage, ThreeChannelsUChar) {
    cv::Mat input_R = (cv::Mat_<uchar>(4, 4) << 0, 1, 2, 3, 1, 2, 3, 4, 2, 3, 4, 5, 3, 4, 5, 6);
    cv::Mat input_G = (cv::Mat_<uchar>(4, 4) << 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3, 0, 1, 2, 3);
    cv::Mat input_B = (cv::Mat_<uchar>(4, 4) << 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
    std::vector<cv::Mat> channels{input_R, input_G, input_B};
    cv::Mat input;
    cv::merge(channels, input);

    cv::Mat expected_R = (cv::Mat_<float>(4, 4) << 0, 1, 3, 6, 1, 4, 9, 16, 3, 9, 18, 30, 6, 16, 30, 48);
    cv::Mat expected_G = (cv::Mat_<float>(4, 4) << 0, 1, 3, 6, 0, 2, 6, 12, 0, 3, 9, 18, 0, 4, 12, 24);
    cv::Mat expected_B = (cv::Mat_<float>(4, 4) << 1, 2, 3, 4, 2, 4, 6, 8, 3, 6, 9, 12, 4, 8, 12, 16);

    cv::Mat received = compute_integral_image(input, -1);
    std::vector<cv::Mat> channels_received;
    cv::split(received, channels_received);
    ASSERT_TRUE(mat_are_equal(expected_R, channels_received[0])) << "Received channel R:\n" << channels_received[0]
                                                                 << "\nFull integral image: "
                                                                 << cv::format(received, cv::Formatter::FMT_NUMPY);
    ASSERT_TRUE(mat_are_equal(expected_G, channels_received[1])) << "Received channel G:\n" << channels_received[1]
                                                                 << "\nFull integral image: "
                                                                 << cv::format(received, cv::Formatter::FMT_NUMPY);
    ASSERT_TRUE(mat_are_equal(expected_B, channels_received[2])) << "Received channel B:\n" << channels_received[2]
                                                                 << "\nFull integral image: "
                                                                 << cv::format(received, cv::Formatter::FMT_NUMPY);
}


int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}