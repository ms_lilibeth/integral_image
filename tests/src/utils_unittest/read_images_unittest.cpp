#include <string>
#include <tuple>
#include <vector>
#include <array>

#include "gtest/gtest.h"
#include "opencv2/core.hpp"

#include "helpers.h"
#include "utils.h"

// A one-color image (all channels have the same value) that should be read from file
struct OneColorImageCheck {
    std::string abs_path;
    int expected_value;

    bool ImageContentEqual(const cv::Mat &other) const {
        cv::Point *pos;
        return cv::checkRange(other, true, pos, expected_value - 1, expected_value + 1);
    }
};

class TestReadImagesOneColor :
        public ::testing::Test,
        public ::testing::WithParamInterface<uint> {
protected:
    std::array<OneColorImageCheck, 3> one_color_images{
            OneColorImageCheck{TestResources::get_abs_resource_path("black.png"), 0},
            OneColorImageCheck{TestResources::get_abs_resource_path("gray_128.png"), 128},
            OneColorImageCheck{TestResources::get_abs_resource_path("white.png"), 255}
    };
    std::vector<std::string> input_paths;

    std::tuple<bool, std::string> MatchesAny(const AnnotatedImage& img_received){
        for (const auto& img_expected: one_color_images){
            if(img_expected.abs_path == img_received.path){
                if(!img_expected.ImageContentEqual(img_received.mat)){
                    return {false, "image contents are not equal"};
                } else{
                    return {true, ""};
                }
            }
        }
        return {false, "unexpected path"};
    }

    void SetUp() override {
        for (const auto &img: one_color_images) {
            input_paths.push_back(img.abs_path);
        }
    }
};

INSTANTIATE_TEST_SUITE_P(
        OneColorImgTest, TestReadImagesOneColor,
        testing::Values(0u, 1u, 2u, 3u, 4u)
);

TEST_P(TestReadImagesOneColor, TestSuccessfulImageReading) {
    std::vector<AnnotatedImage> received = read_images(input_paths);
    for(const auto& img: received){
        auto [match, error_msg] = MatchesAny(img);
        EXPECT_TRUE(match) << img.path << ": " << error_msg;
    }
}

TEST_P(TestReadImagesOneColor, TestNotReturnsFailedImages) {
    input_paths.emplace_back("/path/does/not/exist");
    input_paths.push_back(TestResources::get_abs_resource_path("not-an-image.txt"));
    std::vector<AnnotatedImage> received = read_images(input_paths);
    for(const auto& img: received){
        auto [match, error_msg] = MatchesAny(img);
        EXPECT_TRUE(match) << img.path << ": " << error_msg;
    }
}

// TODO: test on real images of different formats
