#include <filesystem>
#include <string>
#include <fstream>
#include <sstream>
#include <utility>
#include <array>
#include <vector>
#include "gtest/gtest.h"
#include <opencv2/imgcodecs.hpp>

#include "utils.h"
#include "helpers.h"

namespace fs = std::filesystem;

class TestImages2Files :
        public testing::Test,
        public testing::WithParamInterface<uint> {
protected:
    fs::path tmp_output_dir;
    bool failed2start = false;
    std::vector<std::string> images2use{
            "black.png",
            "white.png",
            "gray_128.png",
            "portrait.jpg"
    };
    std::vector<AnnotatedImage> input_data;

    void SetUp() override {
        tmp_output_dir = std::filesystem::temp_directory_path() / ("images2file_unittest_out_" + datetime2str());
        tmp_output_dir /= ("images2file_unittest-th" + std::to_string(GetParam()));
        std::cout << "SetUp " << tmp_output_dir << std::endl;
        if (fs::exists(tmp_output_dir)) {  // temporary files are left after failed test for future examination;
            failed2start = true;
            throw std::runtime_error("Temporary output already exists: " + tmp_output_dir.string());
        }
        fs::create_directories(tmp_output_dir);

        for(const auto& name: images2use){
            AnnotatedImage a_img =  {
                    cv::imread(TestResources::get_abs_resource_path(name), CV_32FC3),
                    (tmp_output_dir / (name + ".integral")).string()
            };
            if(a_img.mat.empty()){
                throw std::runtime_error("Failed to read test image: " + a_img.path);
            }
            input_data.emplace_back(std::move(a_img));
        }
    }

    void TearDown() override {
        if(!(failed2start || testing::Test::HasFailure())){
            fs::remove_all(tmp_output_dir);  // do not remove output file if test failed
        }
    }
};

INSTANTIATE_TEST_SUITE_P(
        ThreadsNumParams, TestImages2Files,
        testing::Values(0u, 1u, 2u, 3u, 4u)
);

TEST_P(TestImages2Files, Images2FileEncodeDecode){
    std::cout << "hello from " << std:: endl;
    std::cout << GetParam() << ' ';
    std::cout << tmp_output_dir << std::endl;
    images2file(input_data, GetParam());
    std::cout << "images2file executed " << GetParam() << std:: endl;
    for(const auto& a_img: input_data){
        std::ifstream file(a_img.path);
        if(!file){
            throw std::runtime_error("Failed to open temporary output file for reading: " + a_img.path);
        }
        auto [success, error_msg] = text_matches_image(file, a_img.mat);
        EXPECT_TRUE(success) << a_img.path << ": " << error_msg;
    }
    std::cout << "finishing " << GetParam() << std:: endl;
}