#include <iostream>
#include <filesystem>
#include "gtest/gtest.h"
#include "helpers.h"

int main(int argc, char **argv) {
    namespace fs = std::filesystem;
    ::testing::InitGoogleTest(&argc, argv);
    if (argc != 2){
        std::cout << "Test resources folder path should be provided" << std::endl;
        return -1;
    }
    const fs::path TEST_RESOURCES(argv[1]);
    try{
        TestResources::test_resources_folder(argv[1]);
    } catch (std::invalid_argument& e){
        std::cout << e.what() << std::endl;
        return -1;
    }
    return RUN_ALL_TESTS();
}