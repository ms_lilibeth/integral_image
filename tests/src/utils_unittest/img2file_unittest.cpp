#include <filesystem>
#include <string>
#include <fstream>
#include <sstream>

#include "gtest/gtest.h"
#include <opencv2/imgcodecs.hpp>

#include "utils.h"
#include "helpers.h"

namespace fs = std::filesystem;

class TestImages2File :
        public ::testing::Test {
protected:
    const fs::path tmp_output_path = std::filesystem::temp_directory_path() /( "img2file_unittest_output_" + datetime2str() + ".txt");
    bool failed2start = false;
    void SetUp() override {
        if (fs::exists(tmp_output_path)) { // temporary files are left after failed test for future examination; TODO: add timestamp to the end to avoid manual remove
            failed2start = true;
            throw std::runtime_error("(Temporary) file already exists: " + tmp_output_path.string());
        }
    }

    void TearDown() override {
        if(!(failed2start || testing::Test::HasFailure())){
            fs::remove(tmp_output_path);  // do not remove output file if test failed
        }
    }
};

TEST_F(TestImages2File, Img2FileEncodeDecode) {
    cv::Mat3f source_img = cv::imread(TestResources::get_abs_resource_path("portrait.jpg"));
    img2file({source_img, tmp_output_path});
    std::ifstream file(tmp_output_path);
    if(!file){
        throw std::runtime_error("Failed to open temporary output file for reading: " + tmp_output_path.string());
    }
    auto [success, error_msg] = text_matches_image(file, source_img);
    ASSERT_TRUE(success) << error_msg;
}

// TODO: test output matches regexp